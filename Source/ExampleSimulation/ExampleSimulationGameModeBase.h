// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ExampleSimulationGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class EXAMPLESIMULATION_API AExampleSimulationGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
